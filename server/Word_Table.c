#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <errno.h> 
#include <string.h>
#include <signal.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h>
#include <termios.h> 
#include <unistd.h>
#include <errno.h>
#include "Word_Table.h"
#include "djb_hash.h"

#define MAXDATASIZE 100



bool PhraseTable_init(PhraseTable_t *pt, size_t size){
    pt->size = size;
    pt->buckets = (Phrase_t **)calloc(size, sizeof(Phrase_t *));
    return pt->buckets != 0;
}

bool ptab_add_phrase(PhraseTable_t *pt, char *word1, char *word2, size_t bucket){
    Phrase_t *newphrase = (Phrase_t *)malloc(sizeof(Phrase_t));
    if (newphrase == NULL){
        return false;
    }
    newphrase->word1 = word1;
    newphrase->word2 = word2;

    newphrase->next = pt->buckets[bucket];
    pt->buckets[bucket] = newphrase;
    return true;
}

Phrase_t *ptab_get_phrase(PhraseTable_t *pt, int bucket){
    return pt->buckets[bucket];
}

void phrase_print(Phrase_t *i) {
    printf("word 1=%s word 2=%s", i->word1, i->word2);
}


void ptab_print(PhraseTable_t *pt) {
    printf("hash table with %lu buckets\n", pt->size);
    for (size_t i = 0; i < pt->size; ++i) {
        printf("bucket %lu: ", i);
        if (pt->buckets[i] == NULL) {
            printf("empty\n");
        } else {
            for (Phrase_t *j = pt->buckets[i]; j != NULL; j = j->next) {
                phrase_print(j);
                if (j->next != NULL) {
                    printf(" -> \n");
                }
            }
            printf("\n");
        }
    }
}

void ptab_destroy(PhraseTable_t *pt){
    for (size_t i=0; i < pt->size; ++i){
        Phrase_t *bucket = pt->buckets[i];
        while (bucket != NULL){
            Phrase_t *next = bucket->next;
            free(bucket);
            bucket = next;
        }            
    }
    free(pt->buckets);
    pt->buckets = NULL;
    pt->size = 0;
}

