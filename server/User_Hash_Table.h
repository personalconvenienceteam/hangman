#ifndef USER_HASH_TABLE_H
#define USER_HASH_TABLE_H

#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <sys/types.h> 

typedef struct User User_t;
struct User{
    char *username;
    char *password;
    User_t *next;
};

typedef struct UserTable UserTable_t;
struct UserTable{
    User_t **buckets;
    size_t size;
};

bool UserTable_init(UserTable_t *ut, size_t size);
size_t utab_index(UserTable_t *ut, char *key);
bool utab_add_user(UserTable_t *ut, char *username, char *password);
User_t *utab_bucket(UserTable_t *ut, char *username);
User_t *utab_find(UserTable_t *ut, char *username);
void user_print(User_t *i);
void utab_print(UserTable_t *ut);
void utab_destroy(UserTable_t *ut);


#endif
