#ifndef DJB_HASH_H
#define DJB_HASH_H

#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <sys/types.h> 

size_t djb_hash(char *s);

#endif
