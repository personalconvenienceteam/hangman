#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <errno.h> 
#include <string.h>
#include <signal.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h>
#include <termios.h> 
#include <unistd.h>
#include <errno.h>

#define DEFAULT_PORT 12345
#define BACKLOG 10
#define MAX_USERNAME_LENGTH 10
#define MAXDATASIZE 100

const char *Authenication_File_Name = "Authentication.txt";
bool Exit_Status = false;

typedef struct User User_t;
struct User{
    char *username;
    char *password;
    User_t *next;
};

typedef struct UserTable UserTable_t;
struct UserTable{
    User_t **buckets;
    size_t size;
};

bool UserTable_init(UserTable_t *ut, size_t size){
    ut->size = size;
    ut->buckets = (User_t **)calloc(size, sizeof(User_t *));
    return ut->buckets != 0;
}

size_t djb_hash(char *s) {
    size_t hash = 5381;
    int c;
    while ((c = *s++) != '\0') {
        // hash = hash * 33 + c
        hash = ((hash << 5) + hash) + c;
    }
    return hash;
}

size_t utab_index(UserTable_t *ut, char *key) {
    return djb_hash(key) % ut->size;
}

bool utab_add_user(UserTable_t *ut, char *username, char *password){
    User_t *newuser = (User_t *)malloc(sizeof(User_t));
    if (newuser == NULL){
        return false;
    }
    newuser->username = username;
    newuser->password = password;

    size_t bucket = utab_index(ut, username);
    newuser->next = ut->buckets[bucket];
    ut->buckets[bucket] = newuser;
    return true;
}

User_t *utab_bucket(UserTable_t *ut, char *username){
    return ut->buckets[utab_index(ut, username)];
}

User_t *utab_find(UserTable_t *ut, char *username){
    for (User_t *i = utab_bucket(ut, username); i != NULL; i = i->next){
        if(strcmp(i->username, username) == 0){
            return i;
        }
    }
    return NULL;
}

void user_print(User_t *i) {
    printf("Username=%s Password=%s", i->username, i->password);
}


void utab_print(UserTable_t *ut) {
    printf("hash table with %d buckets\n", ut->size);
    for (size_t i = 0; i < ut->size; ++i) {
        printf("bucket %d: ", i);
        if (ut->buckets[i] == NULL) {
            printf("empty\n");
        } else {
            for (User_t *j = ut->buckets[i]; j != NULL; j = j->next) {
                user_print(j);
                if (j->next != NULL) {
                    printf(" -> ");
                }
            }
            printf("\n");
        }
    }
}

bool utab_copy_user(UserTable_t *ut, User_t *user){
    User_t *newuser = (User_t *)malloc(sizeof(User_t));
    if (newuser == NULL){
        return false;
    }

    size_t bucket = utab_index(ut, newuser->username);
    newuser->next = ut->buckets[bucket];
    ut->buckets[bucket] = newuser;
    return true;
}

void utab_destroy(UserTable_t *ut){
    for (size_t i=0; i < ut->size; ++i){
        User_t *bucket = ut->buckets[i];
        while (bucket != NULL){
            User_t *next = bucket->next;
            free(bucket);
            bucket = next;
        }            
    }

    free(ut->buckets);
    ut->buckets = NULL;
    ut->size = 0;

}

char Send_String(int socket_id, char *string){
    printf("Sending string; %s\n", string);
    if (send(socket_id, string, sizeof(char)*MAXDATASIZE, 0) == -1){
        perror("send");
        close(socket_id);
        exit(0);
	}
}

void Receive_Client_Input(int socket_id, int size, char *string_dest){
    int bytes_received;
    
    if ((bytes_received=recv(socket_id, string_dest, sizeof(char)*size, 0)) == -1) {
        perror("recv");
        exit(EXIT_FAILURE);			    
	}

    string_dest[bytes_received] = '\0';
    printf("String received %s \n", string_dest);

}

/* Switch input terminal echo on and off */
void Switch_Terminal_Echo(int echo_on){
    struct termios settings;    
    
    if (tcgetattr(STDIN_FILENO, &settings) == -1){
        perror("tcgetattr");
    }
    
    if(echo_on){
        settings.c_lflag |= (ICANON|ECHO);
    }else{
        settings.c_lflag &= ~(ICANON|ECHO);
    }

    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &settings) == -1){
        perror("tcsetattr");    
    }
}

/* Reset terminal and exit gracefully */
void Handle_SIGINT(int signo){
    if (signo  == SIGINT){        
        Exit_Status = true;
    }
}

bool Get_User_Name(int fd, UserTable_t *ut, char *username){
    int i=0;    
    
    while(i<3){
        Receive_Client_Input(fd, MAXDATASIZE, username);       
        printf("Username received: %s \n", username);
        if(utab_find(ut, username) == NULL){
            Send_String(fd, "Invalid Username");
            i++;
        }else{        
            Send_String(fd, username);
            return true;
        }
    }

    return false;
}

bool Get_User_Password(int fd, User_t *ut){
    int i=0;
    char *password = malloc(sizeof(char)*MAXDATASIZE);
    
    while(i<3){
        Receive_Client_Input(fd, MAXDATASIZE, password);       
        printf("Password received: %s \n", password);
        if(strcmp(password, ut -> password) != 0){
            Send_String(fd, "INVALID PASSWORD");
            i++;
        }else{
            Send_String(fd, "Login Success");
            free(password);
            return true;
        }
    }
    free(password);
    return false;
}

bool Load_Auth_Data(FILE *fp, UserTable_t *utab){
    char *buf = malloc(sizeof(char)*MAXDATASIZE);
    size_t len = MAXDATASIZE;
    int line_count = 0;    
    int i=0;

    fp = fopen(Authenication_File_Name, "r");
    while(getline(&buf, &len, fp)!=-1){
        line_count++;
    }
    rewind(fp);
    free(buf);

    for(i=0; i<line_count; i++){    
        char *username = malloc(sizeof(char)*MAXDATASIZE);
        char *password = malloc(sizeof(char)*MAXDATASIZE);
        fscanf(fp, "%s %s", username, password);
//        printf("Username |%s| Password |%s|\n", username, password);
        utab_add_user(utab, username, password);
    }

//    utab_print(utab);
    fclose(fp);
}

int main( int argc, char *argv[]){
    FILE *authentication_file;
    int sock_fd, new_fd, port; /* listen on sock_fd, new connection on new_fd */
    struct sockaddr_in my_addr;    /* This server's address information */
    struct sockaddr_in their_addr; /* Client's address information */    
    struct sigaction exit_handler; /* Signal action structure for handling exit */    
    struct sigaction def_handler;    
    socklen_t sin_size;
    UserTable_t Auth_Data;
    UserTable_t Connected_Users;

    printf("\033c");


    if(!UserTable_init(&Auth_Data, MAXDATASIZE)){
        printf("Could not create User Table\n");
        exit(1);
    }

    if(!UserTable_init(&Connected_Users, MAXDATASIZE)){
        printf("Could not create User Table\n");
        exit(1);
    }

    Load_Auth_Data(authentication_file, &Auth_Data);       

    /* Disable terminal input */
    Switch_Terminal_Echo(0);
    
    /* Handle Ctrl-C exit */
    exit_handler.sa_handler = &Handle_SIGINT;
    if(sigaction(SIGINT, &exit_handler, &def_handler) < 0){
        perror("sigaction");
        exit(1);
    }
 
    /* Get port number for server to listen on */
    if (argc > 1) {
        port = atoi(argv[1]);
    } else {
        port = DEFAULT_PORT;
    }

    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }


    /* generate the end point */
    my_addr.sin_family = AF_INET;         
    my_addr.sin_port = htons(port);       
    my_addr.sin_addr.s_addr = INADDR_ANY; 


    if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) \
        == -1 ){
            perror("setsockopt");
            exit(1);
    }
    
    
    /* bind the socket to the end point */
    if (bind(sock_fd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) \
        == -1) {
            Switch_Terminal_Echo(1);
            perror("bind");
            exit(1);
    }
    
    /* start listnening */
    if (listen(sock_fd, BACKLOG) == -1) {
        perror("listen");
        exit(1);
    }

    printf("Hangman Server is listening on port %d \n", port);

    while(!Exit_Status) {  /* main accept() loop */
        sin_size = sizeof(struct sockaddr_in);
        if ((new_fd = accept(sock_fd, (struct sockaddr *)&their_addr, &sin_size)) == -1) {
            perror("accept");
            continue;
        }

        printf("Client connected from IP: %s \n", inet_ntoa(their_addr.sin_addr));
        Send_String(new_fd, "Connected... \n");                
        
        char *username;

        if (!Get_User_Name(new_fd, &Auth_Data, username)){            
            utab_add_user(&Connected_Users, "TESTING", "FAILED");
        }else{        
            if(!Get_User_Password(new_fd, utab_find(&Auth_Data, username))){
                printf("Invalid login attempt");
                close(new_fd);        
            }        
        }        
        
        close(new_fd);
    }
    close(new_fd);
    close(sock_fd);
    utab_destroy(&Auth_Data);
    utab_destroy(&Connected_Users);

    Switch_Terminal_Echo(1);

    if(sigaction(SIGINT, &def_handler, NULL) < 0){
        perror("sigaction");
        exit(1);
    }

    
    printf("\033c");
    printf("Hangman Server Exited Succesfully\n");
    exit(1);
}
