#ifndef HANGMAN_SERVER_H
#define HANGMAN_SERVER_H

#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include "User_Hash_Table.h"
#include "Connected_Users.h"
#include "Word_Table.h"
#include "Leader_Board.h"


char Send_String(int socket_id, char *string, int size);
void Receive_Client_Input(int socket_id, int size, char *string_dest);
int Receive_Option_Input(int socket_id);
void Switch_Terminal_Echo(int echo_on);
void Handle_SIGINT(int signo);
bool Get_User_Name(int fd, UserTable_t *ut, char *username);
bool Get_User_Password(int fd, User_t *ut);
bool Load_Auth_Data(FILE *fp, UserTable_t *utab);
bool Load_Phrases(FILE *fp, PhraseTable_t *ptab);
bool Connect_New_User(ConUserTable_t *cut, PhraseTable_t *pt, char *username, int socket_id);
void get_new_phrase(ConUser_t *cu, PhraseTable_t *pt);
void Play_Game(ConUser_t *cu, PhraseTable_t *pt, LeaderBoard_t *lbt);
int compare_user_score(const void *left, const void *right);
void Show_Leaderboard(LeaderBoard_t *lbt, int socket_id);
void Disconnect_User(ConUserTable_t *cut, char *username);
bool Run_Option(char option, ConUser_t *cu, PhraseTable_t *pt, LeaderBoard_t *lbt);
void Get_Option_Input(ConUser_t *cu, PhraseTable_t *pt, LeaderBoard_t *lbt);
int main( int argc, char *argv[]);

#endif

