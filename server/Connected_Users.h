#ifndef CONNECTED_USERS_H
#define CONNECTED_USERS_H

#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <sys/types.h> 
#include "Leader_Board.h"

typedef struct ConUser ConUser_t;
struct ConUser{
    char *username;
    char *word1;
    char *word2;
    char guessed[26];
    int score;
    int numguess;
    int socket_id;
    int games_won;
    int games_played;
    ConUser_t *next;
};

typedef struct ConUserTable ConUserTable_t;
struct ConUserTable{
    ConUser_t **buckets;
    size_t size;
};

bool ConUserTable_init(ConUserTable_t *cut, LeaderBoard_t *lbt, size_t size);
size_t cutab_index(ConUserTable_t *cut, char *key);
bool cutab_add_user(ConUserTable_t *cut, char *username, char *word1, char *word2, int socket_id);
ConUser_t *cutab_bucket(ConUserTable_t *cut, char *username);
ConUser_t *cutab_find(ConUserTable_t *cut, char *username);
void cuser_print(ConUser_t *i);
void cutab_print(ConUserTable_t *cut);
void cutab_delete(ConUserTable_t *cut, char *username);
void cutab_destroy(ConUserTable_t *cut);


#endif
