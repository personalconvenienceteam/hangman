#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <errno.h> 
#include <string.h>
#include <signal.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h>
#include <termios.h> 
#include <unistd.h>
#include <errno.h>
#include "User_Hash_Table.h"
#include "djb_hash.h"

#define MAXDATASIZE 100



bool UserTable_init(UserTable_t *ut, size_t size){
    ut->size = size;
    ut->buckets = (User_t **)calloc(size, sizeof(User_t *));
    return ut->buckets != 0;
}

size_t utab_index(UserTable_t *ut, char *key) {
    return djb_hash(key) % ut->size;
}

bool utab_add_user(UserTable_t *ut, char *username, char *password){
    User_t *newuser = (User_t *)malloc(sizeof(User_t));
    if (newuser == NULL){
        return false;
    }
    
    newuser->username = username;
    newuser->password = password;

    size_t bucket = utab_index(ut, username);
    newuser->next = ut->buckets[bucket];
    ut->buckets[bucket] = newuser;
    return true;
}

User_t *utab_bucket(UserTable_t *ut, char *username){
    return ut->buckets[utab_index(ut, username)];
}

User_t *utab_find(UserTable_t *ut, char *username){
    for (User_t *i = utab_bucket(ut, username); i != NULL; i = i->next){
        if(strcmp(i->username, username) == 0){
            return i;
        }
    }
    return NULL;
}

void user_print(User_t *i) {
    printf("Username=%s Password=%s", i->username, i->password);
}


void utab_print(UserTable_t *ut) {
    printf("hash table with %lu buckets\n", ut->size);
    for (size_t i = 0; i < ut->size; ++i) {
        printf("bucket %lu: ", i);
        if (ut->buckets[i] == NULL) {
            printf("empty\n");
        } else {
            for (User_t *j = ut->buckets[i]; j != NULL; j = j->next) {
                user_print(j);
                if (j->next != NULL) {
                    printf(" -> ");
                }
            }
            printf("\n");
        }
    }
}

void utab_destroy(UserTable_t *ut){
    for (size_t i=0; i < ut->size; ++i){
        User_t *bucket = ut->buckets[i];
        while (bucket != NULL){
            User_t *next = bucket->next;
            free(bucket);
            bucket = next;
        }            
    }
    free(ut->buckets);
    ut->buckets = NULL;
    ut->size = 0;
}

