#ifndef WORD_TABLE_H
#define WORD_TABLE_H

#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <sys/types.h> 

typedef struct Phrase Phrase_t;
struct Phrase{
    char *word1;
    char *word2;
    Phrase_t *next;
};

typedef struct PhraseTable PhraseTable_t;
struct PhraseTable{
    Phrase_t **buckets;
    size_t size;
};

bool PhraseTable_init(PhraseTable_t *pt, size_t size);
bool ptab_add_phrase(PhraseTable_t *pt, char *word1, char *word2, size_t bucket);
Phrase_t *ptab_get_phrase(PhraseTable_t *pt, int bucket);
void phrase_print(Phrase_t *i);
void ptab_print(PhraseTable_t *pt);
void ptab_destroy(PhraseTable_t *pt);


#endif
