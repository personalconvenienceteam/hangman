#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <sys/types.h> 
#include "djb_hash.h"

size_t djb_hash(char *s) {
    size_t hash = 5381;
    int c;
    while ((c = *s++) != '\0') {
        // hash = hash * 33 + c
        hash = ((hash << 5) + hash) + c;
    }
    return hash;
}
