#ifndef LEADER_BOARD_H
#define LEADER_BOARD_H

#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <sys/types.h> 

typedef struct UserStat UserStat_t;
struct UserStat{
    char *username;
    int games_won;
    int games_played;
    UserStat_t *next;
};

typedef struct LeaderBoard LeaderBoard_t;
struct LeaderBoard{
    UserStat_t **buckets;
    size_t size;
};

bool LeaderBoard_init(LeaderBoard_t *lbt, size_t size);
size_t lbtab_index(LeaderBoard_t *lbt, char *key);
bool lbtab_add_user(LeaderBoard_t *lbt, char *username);
UserStat_t *lbtab_bucket(LeaderBoard_t *lbt, char *username);
UserStat_t *lbtab_find_username(LeaderBoard_t *lbt, char *username);
void ustat_print(UserStat_t *i);
void lbtab_print(LeaderBoard_t *lbt);
void lbtab_destroy(LeaderBoard_t *lbt);


#endif
