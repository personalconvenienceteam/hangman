#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <errno.h> 
#include <string.h>
#include <signal.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h>
#include <termios.h> 
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include "User_Hash_Table.h"
#include "main.h"
#include "Connected_Users.h"
#include "Word_Table.h"
#include "Leader_Board.h"

#define DEFAULT_PORT 12345
#define BACKLOG 10
#define MAX_USERNAME_LENGTH 10
#define MAXDATASIZE 150

const char *Authenication_File_Name = "Authentication.txt";
const char *Phrase_List_File = "hangman_text.txt";
bool Exit_Status = false;

char Send_String(int socket_id, char *string, int size){
    printf("Sending string; %s\n", string);
    if (send(socket_id, string, sizeof(char)*size, 0) == -1){
        perror("send");
        close(socket_id);
        exit(0);
	}
}

void Receive_Client_Input(int socket_id, int size, char *string_dest){
    int bytes_received;
    
    if ((bytes_received=recv(socket_id, string_dest, sizeof(char)*size, 0)) == -1) {
        perror("recv");
        exit(EXIT_FAILURE);			    
	}

    string_dest[bytes_received] = '\0';
    printf("String received %s \n", string_dest);

}

/* Switch input terminal echo on and off */
void Switch_Terminal_Echo(int echo_on){
    struct termios settings;    
    
    if (tcgetattr(STDIN_FILENO, &settings) == -1){
        perror("tcgetattr");
    }
    
    if(echo_on){
        settings.c_lflag |= (ICANON|ECHO);
    }else{
        settings.c_lflag &= ~(ICANON|ECHO);
    }

    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &settings) == -1){
        perror("tcsetattr");    
    }
}

/* Reset terminal and exit gracefully */
void Handle_SIGINT(int signo){
    if (signo  == SIGINT){        
        Exit_Status = true;
    }
}

bool Get_User_Name(int fd, UserTable_t *ut, char *username){
    int i=0;    

    while(i<3){
        Receive_Client_Input(fd, MAXDATASIZE, username);       
        printf("Username received: %s \n", username);
        if(utab_find(ut, username) != NULL){
            Send_String(fd, username, MAXDATASIZE);
            return true;
        }else{
            Send_String(fd, "Invalid Username", MAXDATASIZE);
            i++;
        }
    }

    return false;
}

bool Get_User_Password(int fd, User_t *ut){
    int i=0;
    char *password = malloc(sizeof(char)*MAXDATASIZE);
    
    while(i<3){
        Receive_Client_Input(fd, MAXDATASIZE, password);       
        printf("Password received: %s \n", password);
        if(strcmp(password, ut -> password) != 0){
            Send_String(fd, "INVALID PASSWORD", MAXDATASIZE);
            i++;
        }else{
            Send_String(fd, "Login Success", MAXDATASIZE);
            free(password);
            return true;
        }
    }
    free(password);
    return false;
}

bool Load_Auth_Data(FILE *fp, UserTable_t *utab){
    char *buf = malloc(sizeof(char)*MAXDATASIZE);
    size_t len = MAXDATASIZE;
    int line_count = 0;    
    int i=0;

    fp = fopen(Authenication_File_Name, "r");
    while(getline(&buf, &len, fp)!=-1){
        line_count++;
    }
    rewind(fp);
    free(buf);
    if(!UserTable_init(utab, line_count)){
        printf("Could not create User Table\n");
        exit(1);
    }


    for(i=0; i<line_count; i++){
        char *username = malloc(sizeof(char)*MAXDATASIZE);
        char *password = malloc(sizeof(char)*MAXDATASIZE);
        fscanf(fp, "%s %s", username, password);
//        printf("Username |%s| Password |%s|\n", username, password);
        utab_add_user(utab, username, password);
    }

//    utab_print(utab);
    fclose(fp);
}

bool Load_Phrases(FILE *fp, PhraseTable_t *ptab){
    char *buf = malloc(sizeof(char)*MAXDATASIZE);
    size_t len = MAXDATASIZE;
    int line_count = 0;    
    int i=0;

    fp = fopen(Phrase_List_File, "r");
    while(getline(&buf, &len, fp)!=-1){
        line_count++;
    }
    rewind(fp);
    free(buf);
    
    if(!PhraseTable_init(ptab, line_count)){
        printf("Could not create User Table\n");
        exit(1);
    }

    for(i=0; i<line_count; i++){    
        char *word1 = malloc(sizeof(char)*MAXDATASIZE);
        char *word2 = malloc(sizeof(char)*MAXDATASIZE);
        fscanf(fp, "%[^ ,], %s\n", word1, word2);
        ptab_add_phrase(ptab, word1, word2, (size_t)i);
    }

//    ptab_print(ptab);
    fclose(fp);
}

bool Connect_New_User(ConUserTable_t *cut, PhraseTable_t *pt, char *username, int socket_id){
    Phrase_t *newphrase = malloc(sizeof(Phrase_t));
    char *word1 = malloc(sizeof(char)*MAXDATASIZE);
    char *word2 = malloc(sizeof(char)*MAXDATASIZE);
    int bucket, maxbucket;

    maxbucket = (int)pt->size;
    bucket = rand() % maxbucket;

    newphrase = ptab_get_phrase(pt, bucket);

    word1 = newphrase -> word1;
    word2 = newphrase -> word2;

    if(!cutab_add_user(cut, username, word1, word2, socket_id)){
        printf("ERROR: Could not add user");
        return false;
    }
}

void Obscure_String (char *phrase, char *obscured, char *guessed){
    int i=0, j=0;
    char *obscuremap = calloc(26, sizeof(char));
    
    for (i=0; i<26; i++){
        for (j=0; j<strlen(phrase); j++){
            if(guessed[i] == phrase[j]&&obscuremap[j]!='1'){
                obscuremap[j]='1';
            }
        }
    }

    i = 0;
    
    for(i=0; i<strlen(phrase); i++){
        if(obscuremap[i] == '1' || phrase[i]==' '){
            obscured[i]=phrase[i];
        }else{
            obscured[i]='_';
        }                    
    }

    free(obscuremap);
}

void get_new_phrase(ConUser_t *cu, PhraseTable_t *pt){
    Phrase_t *newphrase = malloc(sizeof(Phrase_t));
    int bucket, maxbucket;

    maxbucket = (int)pt->size;
    bucket = rand() % maxbucket;

    newphrase = ptab_get_phrase(pt, bucket);

    cu->word1 = newphrase -> word1;
    cu->word2 = newphrase -> word2;    
}

void Play_Game(ConUser_t *cu, PhraseTable_t *pt, LeaderBoard_t *lbt){
    char *game_str = malloc(sizeof(char)*MAXDATASIZE);
    char *phrase = malloc(sizeof(char)*MAXDATASIZE);
    char *obscured = calloc(26, sizeof(char));
    char *play_state = malloc(sizeof(char));
    char *guess = malloc(sizeof(char));
    UserStat_t *ustat = malloc(sizeof(UserStat_t));
    int i=0;

    if(cu->games_played > 0){
        get_new_phrase(cu, pt);
        memset(cu->guessed, '\0', 26);
    }

    sprintf(phrase, "%s %s", cu -> word1, cu -> word2);
    play_state = "PLAY";
    Obscure_String(phrase, obscured, cu->guessed);
    cu -> games_played = cu->games_played + 1;
    
    if(lbt->size == 0){    
        lbt->size = lbt->size + 1;
        lbtab_add_user(lbt, cu->username);
    }

    if((ustat = lbtab_find_username(lbt, cu->username))==NULL){
        lbt->size = lbt->size + 1;
        lbtab_add_user(lbt, cu->username);
        ustat = lbtab_find_username(lbt, cu->username);
    }

    ustat -> games_played = ustat -> games_played + 1;

    while(strcmp(play_state, "PLAY")==0){    
        lbtab_print(lbt);
        printf("Phrase: %s \n", phrase);
        sprintf(game_str, "Guessed letters: %s \n" \
                          "\n"\
                          "Number of guesses left: %u\n" \
                          "\n"\
                          "Word: %s\n", cu->guessed, \
                          cu->numguess - i, obscured);

        Send_String(cu->socket_id, game_str, MAXDATASIZE);

        Receive_Client_Input(cu->socket_id, 1, guess);
        cu->guessed[i]=guess[0];
        i++;
        Obscure_String(phrase, obscured, cu->guessed);
        if((cu->numguess - i)==0){
            play_state="LOSE";
        }
        if(strcmp(obscured, phrase) == 0){
            play_state="WINS";
            cu->games_won = cu->games_won+1;
            ustat -> games_won = ustat -> games_won + 1;
        lbtab_print(lbt);
        }
        Send_String(cu->socket_id, play_state, 4);
        if(strcmp(play_state, "WINS") == 0){
            sprintf(game_str, "Your phrase was: %s \n", phrase);
            Send_String(cu->socket_id, game_str, MAXDATASIZE);
        }
    }
    
}

void sort_leader_board(LeaderBoard_t *lbt){
    int i=0, j=0;
    bool sort = true;
    UserStat_t *stat1 = malloc(sizeof(UserStat_t));
    UserStat_t *stat2 = malloc(sizeof(UserStat_t));
    LeaderBoard_t *templb = malloc(sizeof(LeaderBoard_t));
    LeaderBoard_init(templb, lbt->size);
        
    while(sort){
        sort = false;
        for(i=0; i<lbt->size - 1; i++){
            j = i + 1;
            stat1 = lbt->buckets[i];
            stat2 = lbt->buckets[j];
            if(stat1->games_won < stat2->games_won){
                templb->buckets[i]=stat2;
                templb->buckets[j]=stat1;
                sort = true;
            }else{
                templb->buckets[i]=stat1;
                templb->buckets[j]=stat2;
            }
        }
        
        i = 0;
        for(i=0; i<lbt->size; i++){
            lbt->buckets[i] = templb->buckets[i];
        }
    }
}

void Show_Leaderboard(LeaderBoard_t *lbt, int socket_id){
    char *lb_string = malloc(sizeof(char)*MAXDATASIZE);
    UserStat_t *curstat = malloc(sizeof(UserStat_t));
    if(lbt->size > 1){
        lbtab_print(lbt);
        sort_leader_board(lbt);
        lbtab_print(lbt);
    }

    if(lbt->size == 0){
        Send_String(socket_id, "0", 1);
        Receive_Client_Input(socket_id, 4, lb_string);
        return;
    }else{
        int i=0;
        sprintf(lb_string, "%lu", lbt->size);
        Send_String(socket_id, lb_string, strlen(lb_string));
        for(i=0; i<lbt->size; i++){
            curstat = lbt->buckets[i];
            sprintf(lb_string, "============================\n" \
                               "\n" \
                               "Player - %s\n" \
                               "Number of Games won - %d\n" \
                               "Number of Games played - %d\n"\
                               "\n" \
                               "============================\n", \
                       curstat->username, curstat->games_won,\
                       curstat->games_played);
            Send_String(socket_id, lb_string, MAXDATASIZE);
            Receive_Client_Input(socket_id, 4, lb_string);
        }
    }

}

void Disconnect_User(ConUserTable_t *cut, char *username){
    printf("User - %s - disconnected\n", username);
    cutab_delete(cut, username);
}

bool Run_Option(char option, ConUser_t *cu, PhraseTable_t *pt, LeaderBoard_t *lbt){
    bool play;    
    switch(option){
        case '1' :
            Play_Game(cu, pt, lbt);
            play = true;
            break;
        case '2' :
            Show_Leaderboard(lbt, cu->socket_id);
            play = true;
            break;
        case '3' :
            play = false;
            break;
    }
    return play;
}

void Get_Option_Input(ConUser_t *cu, PhraseTable_t *pt, LeaderBoard_t *lbt){
    char *useroption = malloc(sizeof(char));
    bool play = true;

    do{
        Receive_Client_Input(cu->socket_id, 1, useroption);
        play = Run_Option(useroption[0], cu, pt, lbt);
        printf("Now I'm back here\n");
    }while(play);
}

int main( int argc, char *argv[]){
    FILE *authentication_file;
    FILE *phrase_file;
    int sock_fd, new_fd, port; /* listen on sock_fd, new connection on new_fd */
    struct sockaddr_in my_addr;    /* This server's address information */
    struct sockaddr_in their_addr; /* Client's address information */    
    struct sigaction exit_handler; /* Signal action structure for handling exit */    
    struct sigaction def_handler;
    size_t size;    
    socklen_t sin_size;
    UserTable_t Auth_Data;
    ConUserTable_t Connected_Users;
    LeaderBoard_t LeaderBoard;
    PhraseTable_t Phrases;
    time_t t;    

    srand((unsigned) time(&t));

    printf("\033c");

    Load_Auth_Data(authentication_file, &Auth_Data);       
    size = Auth_Data.size;
    if(!ConUserTable_init(&Connected_Users, &LeaderBoard, size)){
        printf("Could not create User Table\n");
        exit(1);
    }
    
    Load_Phrases(phrase_file, &Phrases);

    /* Disable terminal input */
    Switch_Terminal_Echo(0);
    
    /* Handle Ctrl-C exit */
    exit_handler.sa_handler = &Handle_SIGINT;
    if(sigaction(SIGINT, &exit_handler, &def_handler) < 0){
        perror("sigaction");
        exit(1);
    }
 
    /* Get port number for server to listen on */
    if (argc > 1) {
        port = atoi(argv[1]);
    } else {
        port = DEFAULT_PORT;
    }

    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }


    /* generate the end point */
    my_addr.sin_family = AF_INET;         
    my_addr.sin_port = htons(port);       
    my_addr.sin_addr.s_addr = INADDR_ANY; 


    if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) \
        == -1 ){
            perror("setsockopt");
            exit(1);
    }
    
    
    /* bind the socket to the end point */
    if (bind(sock_fd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) \
        == -1) {
            Switch_Terminal_Echo(1);
            perror("bind");
            exit(1);
    }
    
    /* start listnening */
    if (listen(sock_fd, BACKLOG) == -1) {
        perror("listen");
        exit(1);
    }

    printf("Hangman Server is listening on port %d \n", port);

    while(!Exit_Status) {  /* main accept() loop */
        sin_size = sizeof(struct sockaddr_in);
        if ((new_fd = accept(sock_fd, (struct sockaddr *)&their_addr, &sin_size)) == -1) {
            perror("accept");
            continue;
        }

        printf("Client connected from IP: %s \n", inet_ntoa(their_addr.sin_addr));
        Send_String(new_fd, "Connected... \n", MAXDATASIZE);                
        
        char *username = malloc(sizeof(char)*MAXDATASIZE);
        ConUser_t *CurUser = malloc(sizeof(ConUser_t));

        if (!Get_User_Name(new_fd, &Auth_Data, username)){            
                printf("Invalid login attempt");
                close(new_fd);        
        }else{
            if(!Get_User_Password(new_fd, utab_find(&Auth_Data, username))){
                printf("Invalid login attempt");
                close(new_fd);        
            }else{
                Connect_New_User(&Connected_Users, &Phrases, username, new_fd);
                CurUser = cutab_find(&Connected_Users, username);
            }
        }

        Get_Option_Input(CurUser, &Phrases, &LeaderBoard);

        Disconnect_User(&Connected_Users, username);

        close(new_fd);

        printf("Listening for new user\n");
    }
    close(new_fd);
    close(sock_fd);
    utab_destroy(&Auth_Data);
    cutab_destroy(&Connected_Users);
    ptab_destroy(&Phrases);
    lbtab_destroy(&LeaderBoard);
    Switch_Terminal_Echo(1);

    if(sigaction(SIGINT, &def_handler, NULL) < 0){
        perror("sigaction");
        exit(1);
    }

    
    printf("\033c");
    printf("Hangman Server Exited Succesfully\n");
    exit(1);
}
