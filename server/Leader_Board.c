#include <arpa/inet.h>
#include <math.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <errno.h> 
#include <string.h>
#include <signal.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h>
#include <termios.h> 
#include <unistd.h>
#include <errno.h>
#include "Leader_Board.h"
#include "djb_hash.h"

#define MAXDATASIZE 100



bool LeaderBoard_init(LeaderBoard_t *lbt, size_t size){
    lbt->size = size;
    lbt->buckets = (UserStat_t **)calloc(size, sizeof(UserStat_t *));
    return lbt->buckets != 0;
}

size_t lbtab_index(LeaderBoard_t *lbt, char *key) {
    return djb_hash(key) % lbt->size;
}

bool lbtab_add_user(LeaderBoard_t *lbt, char *username){
    UserStat_t *newuser = (UserStat_t *)malloc(sizeof(UserStat_t));
    if (newuser == NULL){
        return false;
    }
    
    newuser->username = malloc(strlen(username)+1);
    strcpy(newuser->username, username);
    newuser->games_played = 0;
    newuser->games_won = 0;
    
    int bucket = lbt->size - 1;
    newuser->next = lbt->buckets[bucket];
    lbt->buckets[bucket] = newuser;
    return true;
}



UserStat_t *lbtab_bucket(LeaderBoard_t *lbt, char *username){
    return lbt->buckets[lbtab_index(lbt, username)];
}

UserStat_t *lbtab_find_username(LeaderBoard_t *lbt, char *username){
    UserStat_t *UserStat = malloc(sizeof(UserStat_t));
    for (int i = 0; i < lbt->size; i++){
        UserStat = lbt->buckets[i];

        if(strcmp(UserStat->username, username) == 0){
            return UserStat;
        }
    }
    return NULL;
}

void ustat_print(UserStat_t *i){
    printf("Username=%s Games Won=%d Games Played=%d", \
            i->username, i->games_won, i->games_played);
}


void lbtab_print(LeaderBoard_t *lbt){
    for (size_t i = 0; i < lbt->size; ++i) {
        printf("bucket %lu: ", i);
        if (lbt->buckets[i] == NULL) {
            printf("empty\n");
        } else {
            for (UserStat_t *j = lbt->buckets[i]; j != NULL; j = j->next) {
                ustat_print(j);
                if (j->next != NULL) {
                    printf(" -> ");
                }
            }
            printf("\n");
        }
    }
}

void lbtab_destroy(LeaderBoard_t *lbt){
    for (size_t i=0; i < lbt->size; ++i){
        UserStat_t *bucket = lbt->buckets[i];
        while (bucket != NULL){
            UserStat_t *next = bucket->next;
            free(bucket);
            bucket = next;
        }            
    }
    free(lbt->buckets);
    lbt->buckets = NULL;
    lbt->size = 0;
}

