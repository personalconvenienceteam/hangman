#include <arpa/inet.h>
#include <math.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <errno.h> 
#include <string.h>
#include <signal.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h>
#include <termios.h> 
#include <unistd.h>
#include <errno.h>
#include "Connected_Users.h"
#include "Leader_Board.h"
#include "djb_hash.h"

#define MAXDATASIZE 100



bool ConUserTable_init(ConUserTable_t *cut, LeaderBoard_t *lbt, size_t size){
    cut->size = size;
    cut->buckets = (ConUser_t **)calloc(size, sizeof(ConUser_t *));
    LeaderBoard_init(lbt, 0);
    return cut->buckets != 0;
}

size_t cutab_index(ConUserTable_t *cut, char *key) {
    return djb_hash(key) % cut->size;
}

bool cutab_add_user(ConUserTable_t *cut, char *username, char *word1, char *word2, int socket_id){
    int numguesses;
    ConUser_t *newuser = malloc(sizeof(ConUser_t));
    if (newuser == NULL){
        return false;
    }
    newuser->username = username;
    newuser->word1 = word1;
    newuser->word2 = word2;    
    newuser->score = 0;
    newuser->socket_id = socket_id;
    newuser->games_played = 0;
    newuser->games_won = 0;
    memset(newuser->guessed, '\0', 26);    
    if((numguesses = strlen(word1)+strlen(word2)+10) < 26){
        newuser->numguess = numguesses;
    }else{
        newuser->numguess = 26;
    }    

    size_t bucket = cutab_index(cut, username);
    newuser->next = cut->buckets[bucket];
    cut->buckets[bucket] = newuser;
    return true;
}

ConUser_t *cutab_bucket(ConUserTable_t *cut, char *username){
    return cut->buckets[cutab_index(cut, username)];
}

ConUser_t *cutab_find(ConUserTable_t *cut, char *username){
    for (ConUser_t *i = cutab_bucket(cut, username); i != NULL; i = i->next){
        if(strcmp(i->username, username) == 0){
            return i;
        }
    }
    return NULL;
}

void cuser_print(ConUser_t *i){
    printf("Username=%s phrase=%s %s Score=%d Guesses=%d", \
            i->username, i->word1, i->word2, i->score, i->numguess);
}


void cutab_print(ConUserTable_t *cut){
    for (size_t i = 0; i < cut->size; ++i) {
        printf("bucket %lu: ", i);
        if (cut->buckets[i] == NULL) {
            printf("empty\n");
        } else {
            for (ConUser_t *j = cut->buckets[i]; j != NULL; j = j->next) {
                cuser_print(j);
                if (j->next != NULL) {
                    printf(" -> ");
                }
            }
            printf("\n");
        }
    }
}

void cutab_delete(ConUserTable_t *cut, char *username){
    ConUser_t *head = cutab_bucket(cut, username);
    ConUser_t *current = head; 
    ConUser_t *previous = NULL;

    while (current != NULL) {
        if (strcmp(current->username, username) == 0) {

            if (previous == NULL) { // first item in list
                cut->buckets[cutab_index(cut, username)] = current->next;
            } else {
                previous->next = current->next;
            }
            free(current);
            break;
        }
        previous = current;
        current = current->next;
    }
}


void cutab_destroy(ConUserTable_t *cut){
    for (size_t i=0; i < cut->size; ++i){
        ConUser_t *bucket = cut->buckets[i];
        while (bucket != NULL){
            ConUser_t *next = bucket->next;
            free(bucket);
            bucket = next;
        }            
    }
    free(cut->buckets);
    cut->buckets = NULL;
    cut->size = 0;
}

