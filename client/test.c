#include <stdio.h> 
#include <stdlib.h>

int main(){
    char *ptr1 = malloc(sizeof(char));
    char *ptr2 = malloc(sizeof(char));

    printf("ptr1 = %x \n", &ptr1);
    printf("ptr2 = %x \n", &ptr2);
    
    ptr1 = "ABCDEFGHIJ";
    *ptr2 = *ptr1;

    printf("ptr2 = %x \n", &ptr2);


    free(ptr1);
    free(ptr2);
}
