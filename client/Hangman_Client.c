#include <stdio.h> 
#include <stdlib.h>
#include <stdbool.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <ctype.h>

#define DEFAULT_PORT 12345
#define MAXDATASIZE 150

const char TITLE_SPLASH[] = "====================================\n" \
                            "\n" \
                            "Welcome to Hangman 9000: Online GoD!\n" \
                            "\n" \
                            "====================================\n";

const char MENU[] = "Please enter a selection\n" \
                    "<1> Play Hangman\n"         \
                    "<2> Show Leaderboard\n"     \
                    "<3> Quit\n"                 \
                    "\n";                         
  
void Send_String(int socket_id, char *string, int size) {
    if (send(socket_id, string, sizeof(char)*size, 0) == -1){
        perror("send");
        close(socket_id);
        exit(0);
	}
}

char *Receive_Server_String(int socket_id, int size){
    int bytes_received;
    
    char *received_string = malloc(sizeof(char)*MAXDATASIZE);
    if ((bytes_received=recv(socket_id, received_string, sizeof(char)*size, 0)) == -1) {
        perror("recv");
        exit(EXIT_FAILURE);			    
	}

    received_string[bytes_received] = '\0';

	return received_string;
}

bool Get_Username(int socket_id, char *username){
    int i=0;
    char *check_username = malloc(sizeof(char)*MAXDATASIZE);

    do{
        printf("Enter your Username:  ");

        scanf("%s", username);
        Send_String(socket_id, username, MAXDATASIZE);
        check_username = Receive_Server_String(socket_id, MAXDATASIZE);
    
        if (strcmp(username, check_username) == 0){
            free(check_username);            
            return true;
        }else if (i<2){
            printf("Invalid Username, please try again\n");
            i++;    
        }else{
            i++;        
        }
    }while(i<3);
    free(check_username);
    return false;
} 

bool Enter_Password(int socket_id, char *username){
    char *password = malloc(sizeof(char)*MAXDATASIZE);
    char *check_pw = malloc(sizeof(char)*MAXDATASIZE);
    int i=0;

    while(i<3){
        printf("Hello, %s Please enter your password:", username);
        scanf("%s", password);
        Send_String(socket_id, password, MAXDATASIZE);    
        check_pw = Receive_Server_String(socket_id, MAXDATASIZE);
    
        if (strcmp(check_pw, "INVALID PASSWORD") == 0){
            i++;
        }else if(strcmp(check_pw, "Login Success") == 0){
            free(password);
            free(check_pw);
            return true;            
        }
    }
    return false;
}

char Show_Menu(){
    int option;
    char test[1];
    
    printf(MENU);
    printf("Choose an option 1-3 ->");        

    scanf("%d", &option);

    if(option != 1 && option && option != 2 && option != 3){
        printf("%d is not a valid option...", option);
        option = Show_Menu();
    }
    sprintf(test, "%d", option);
    return *test;
}

void Play_Game(int socket_id){
    char *game_str = malloc(sizeof(char)*MAXDATASIZE);
    char *guess = malloc(sizeof(char));
    char *play_state = malloc(sizeof(char));

    play_state = "PLAY";

    do{
        game_str = Receive_Server_String(socket_id, MAXDATASIZE);
        printf("%s\n", game_str);
        printf("Guess a letter: ");
        scanf("%s", guess);
        printf("\n");
        guess[0] = tolower(guess[0]);
        Send_String(socket_id, guess, 1);
        play_state = Receive_Server_String(socket_id, 4);
    }while(strcmp(play_state, "PLAY")==0);

    if(strcmp(play_state, "WINS") == 0){
        game_str = Receive_Server_String(socket_id, MAXDATASIZE);
        printf("Congratulations! You win!\n" \
               "%s \n", game_str);
    }

    if(strcmp(play_state, "LOSE") ==0){
        printf("You lost! Better luck next time!\n");    
    }
    
    free(game_str);
    free(guess);
    free(play_state);
}

void Show_Leaderboard(int sock_fd){
    char *statstr = malloc(sizeof(char) * MAXDATASIZE);
    char *lb_string = malloc(sizeof(char) * MAXDATASIZE);
    int numstats;
    
    statstr = Receive_Server_String(sock_fd, MAXDATASIZE);
    numstats = atoi(statstr);
    if(numstats == 0){
            lb_string = "===================================\n"\
                        "\n"\
                        "   No Leader Board available yet\n"   \
                        "       No users have played \n"       \
                        "\n"\
                        "===================================\n";
            printf("%s\n", lb_string);
            Send_String(sock_fd, "NEXT", 4);
    }else{
        int i=0;
        for (i=0; i<numstats; i++){
            lb_string = Receive_Server_String(sock_fd, MAXDATASIZE);
            printf("%s \n", lb_string);
            Send_String(sock_fd, "NEXT", 4);
        }
    }
}

bool Run_Option(char option, int socket_id){
    switch(option){
        case '1' :
            Play_Game(socket_id);
            return true;
        case '2' :
            Show_Leaderboard(socket_id);
            return true;
        case '3' :
            printf("Exiting...\n");
            return false;
    }
}

int main(int argc, char *argv[]) {
    int sock_fd, numbytes, port;  
    char buf[MAXDATASIZE], *username;
    struct hostent *he;
    struct sockaddr_in their_addr; /* connector's address information */
    
    printf("\033c");

    if (argc < 2) {
        fprintf(stderr,"usage: client hostname\n");
        exit(1);
    }

    if (argc == 3) {
        port = atoi(argv[2]);
    } else {
        port = DEFAULT_PORT;
    }

	if ((he=gethostbyname(argv[1])) == NULL) {  /* get the host info */
		herror("gethostbyname");
		exit(1);
	}

	if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	their_addr.sin_family = AF_INET;      /* host byte order */
	their_addr.sin_port = htons(port);    /* short, network byte order */
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8);     /* zero the rest of the struct */

	if (connect(sock_fd, (struct sockaddr *)&their_addr, \
	sizeof(struct sockaddr)) == -1) {
		perror("connect");
		exit(1);
	}

	if ((numbytes=recv(sock_fd, buf, MAXDATASIZE, 0)) == -1) {
		perror("recv");
		exit(1);
	}

	buf[numbytes] = '\0';

    printf(TITLE_SPLASH);    
	printf("Connected to server... \n\n");
    username = malloc(sizeof(char));
    if(!Get_Username(sock_fd, username)){
        printf("Invalid login attempt, exiting\n");
        close(sock_fd);        
        exit(1);
    }    

    if(!Enter_Password(sock_fd, username)){
        printf("Invalid login attempt, exiting\n");
        close(sock_fd);        
        exit(1);
    }
    bool play = true;
    do{
        char option[1];
        char *okay = malloc(sizeof(char)*4);
        option[0] = Show_Menu();
        Send_String(sock_fd, &option[0], 1);
        
        play = Run_Option(option[0], sock_fd);
    }while(play);

    exit(1);
}
